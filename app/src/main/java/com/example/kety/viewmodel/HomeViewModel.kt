package com.example.kety.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.kety.model.KetyRepo

class HomeViewModel : ViewModel() {

    private val repo = KetyRepo

    private val _items = MutableLiveData<List<List<String>>>()
    val items : LiveData<List<List<String>>> get() = _items


    init{
        val items = repo.getItems()
        _items.value = items
    }


}