package com.example.kety.model

object KetyRepo {

    private val itemsList = listOf(
        listOf(
            "RANK",
            "HOT",
            "LOVED",
            "SECRETS",
            "DANK",
            "COLD",
            "HATED",
            "KNOWN",
            "TYPE",
            "PLACE"
        ),
        listOf(
            "NORMAL",
            "DRY",
            "OILY",
            "COMBINE",
            "HARD",
            "SOFT",
            "FIRM",
            "SCALES",
            "COUP",
            "BONES?"
        )
    )

    fun getItems(): List<List<String>> {
        return itemsList
    }
}